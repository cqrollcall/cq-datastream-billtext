package com.cq.datastream;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.client.RestTemplate;

import com.cq.alex.api.v1.type.Document;
import com.cq.alex.api.v1.type.DocumentMeta;
import com.cq.alex.api.v1.type.DocumentSearchHit;
import com.cq.alex.api.v1.type.DocumentSearchRequest;
import com.cq.alex.api.v1.type.DocumentSearchResponse;

import oracle.jdbc.pool.OracleDataSource;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude={JdbcTemplateAutoConfiguration.class, DataSourceAutoConfiguration.class})
@Configuration
public class BillTextRunner 
{
  
  private Logger log = LogManager.getLogger(CommandLineRunner.class);
  
  @Autowired
  private Environment env;
  
  private JdbcTemplate readTemplate;
  
  private JdbcTemplate writeTemplate;
  
  private String table;
  
  private final String APPROPRIATIONS_SQL="select p.ft_group_code as code, p.full_name as label from CQAPPS.pubgroup p, CQAPPS.product_pubgroup pp, cqapps.product_cycle_ref pc where p.pubgroup_id = pp.pubgroup_id and pp.product_type_id = 4 and p.product_cycle_type_id = pc.product_cycle_type_id and pc.ft_product_cycle_code = 'FY21' order by p.full_name";
    
  public static void main(String[] args) {
    new SpringApplicationBuilder(BillTextRunner.class).web(WebApplicationType.NONE).build().run(args);
  }  
  

  @Bean
  public void readTemplate() throws SQLException{
    log.info("Login to Read DB with user "+env.getProperty("read.jdbc.username"));
    OracleDataSource dataSource = new OracleDataSource();
    dataSource.setURL(env.getProperty("read.jdbc.url"));
    dataSource.setUser(env.getProperty("read.jdbc.username"));
    dataSource.setPassword(env.getProperty("read.jdbc.password"));

    readTemplate = new JdbcTemplate(dataSource);
  } 
  

  @Bean
  public void writeTemplate() throws SQLException{
    log.info("Login to Write DB with user "+env.getProperty("write.jdbc.username"));
    OracleDataSource dataSource = new OracleDataSource();
    dataSource.setURL(env.getProperty("write.jdbc.url"));
    dataSource.setUser(env.getProperty("write.jdbc.username"));
    dataSource.setPassword(env.getProperty("write.jdbc.password"));

    writeTemplate = new JdbcTemplate(dataSource);
  }   
    
  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    log.info("Login to Alex with user "+env.getProperty("alex.login.username"));
    builder.basicAuthentication(env.getProperty("alex.login.username"), env.getProperty("alex.login.password"));
    RestTemplate restTemplate = builder.build();
    
    List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();
    
    if(interceptors == null)
      interceptors = new ArrayList<ClientHttpRequestInterceptor>();

    interceptors.add(new ClientHttpRequestInterceptor() {
      
      @Override
      public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders currHeaders = request.getHeaders();
        currHeaders.add("cqrcUserId", env.getProperty("alex.api.userid"));
        return execution.execute(request, body);
      }
    });
    restTemplate.setInterceptors(interceptors);
    
    return restTemplate;
  }
  
  @Bean
  public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
    return args -> {
      try {
        String apiBase = env.getProperty("alex.api.base");        
        
        table = env.getProperty("target.table");
        if(table == null || table.equals("")){
          SimpleDateFormat df = new SimpleDateFormat("YYYYMMdd");
          table = "TMP_LT_BILL_TEXT_"+df.format(new Date());
          
          table = env.getProperty("write.jdbc.username") + "." + table;
          table = table + "_" + env.getActiveProfiles()[0];
        }
        
        boolean createTable = false;
        
        try{
          writeTemplate.query("select * from " + table, new RowMapper<String>(){

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
              // TODO Auto-generated method stub
              return null;
            }
            
          });
        }catch(Exception e){
          log.info("Table doesn't exist");
          createTable= true;
        }
        
        log.info("Table ===> " + table + ",createTable ==> " + createTable);
        
        if(createTable){
          String tableCreationScript = "CREATE TABLE " + table
              + " (DOC_UID VARCHAR2(50 BYTE), "
              + "DOCUMENT_ID NUMBER, "
              + "BILL_NUMBER VARCHAR2(4000 BYTE), "
              + "OFF_TITLE VARCHAR2(4000 BYTE), "
              + "VERSION_CODE VARCHAR2(400 BYTE), "
              + "VERSION_TYPE VARCHAR2(400 BYTE), "
              + "RECORD_ID VARCHAR2(400 BYTE), "
              + "APPROPRIATION VARCHAR2(200 BYTE), "
              + "CHAMBER VARCHAR2(100 BYTE), "
              + "COMMITTEE VARCHAR2(500 BYTE), "
              + "FISCAL_YEAR VARCHAR2(10 BYTE), "
              + "WORD_COUNT_META NUMBER(10,0), "
              + "WORD_COUNT_ACTUAL NUMBER(10,0), "
              + "LOAD_ID VARCHAR2(150 BYTE), "
              + "LEGISLATIVE_TYPE VARCHAR2(500 BYTE))";
          
          writeTemplate.execute(tableCreationScript);
          
     //     System.out.println("Executing grant command for table --> " + table);
     //     writeTemplate.execute("GRANT SELECT ON " + table + " TO DEVELOPER_ROLE");
        }
        
//        String threadSizeStr = env.getProperty("runner.thread.size");
//        Integer threadSize = Integer.parseInt(threadSizeStr);
        
        // Appropriations
        List<Appropriation> appropsList = readTemplate.query(APPROPRIATIONS_SQL, new RowMapper<Appropriation>(){

          @Override
          public Appropriation mapRow(ResultSet rs, int rowNum) throws SQLException {
            Appropriation app = new Appropriation();
            app.setCode(rs.getString("CODE"));
            app.setLabel(rs.getString("LABEL"));
            return app;
          }
          
        });
        
        
        for (Iterator iterator = appropsList.iterator(); iterator.hasNext();) {
          Appropriation appropriation = (Appropriation) iterator.next();
          
          log.info("Running Appropriation Entries: " + appropriation.getLabel());
          
          String cqql = prepareCQQL("approps.cqql", new HashMap<String,String>(){
            {
              put("CQ_CONGRESS_NUMBER","116");
              put("CQ_BILL_GROUP", appropriation.getCode());
            }
          });
          
          extractAndInsert(restTemplate, apiBase, table, cqql, appropriation.getLabel(), null, null, null, null);
          
        }
        
        // Chamber Type
        for (Chamber type : Chamber.values()) {
          
          log.info("Running Chamber Entries: " + type.getLabel());
          
          String cqql = prepareCQQL("chamber_type.cqql", new HashMap<String,String>(){
            {
              put("CQ_CONGRESS_NUMBER","116");
              put("CQ_CHAMBER_TYPE", type.name());
            }
          });
          
          extractAndInsert(restTemplate, apiBase, table, cqql, null, type.getLabel(), null, null, null);
          
        }
        
        // Committees
        for (Committee type : Committee.values()) {
          
          log.info("Running Committee Entries: " + type.getLabel());
          
          String cqql = prepareCQQL("commitees.cqql", new HashMap<String,String>(){
            {
              put("CQ_CONGRESS_NUMBER","116");
              put("CQ_COMMITTEE", type.name());
            }
          });
          
          extractAndInsert(restTemplate, apiBase, table, cqql, null, null, type.getLabel(), null, null);
          
        }
        
        // Sub Committees
        for (SubCommittee type : SubCommittee.values()) {
          
          log.info("Running Sub Committee Entries: " + type.getLabel());
          
          String cqql = prepareCQQL("commitees.cqql", new HashMap<String,String>(){
            {
              put("CQ_CONGRESS_NUMBER","116");
              put("CQ_COMMITTEE", type.name());
            }
          });
          
          extractAndInsert(restTemplate, apiBase, table, cqql, null, null, type.getLabel(), null, null);
          
        }
        
        // Legislative Stage Type
        for (LegislativeType type : LegislativeType.values()) {
          
          log.info("Running Sub Committee Entries: " + type.getText());
          
          String cqql = prepareCQQL("legislative_type.cqql", new HashMap<String,String>(){
            {
              put("CQ_CONGRESS_NUMBER","116");
              put("CQ_LEGISLATIVE_TYPE", type.getValue());
            }
          });
          
          extractAndInsert(restTemplate, apiBase, table, cqql, null, null, null, null, type.getText());
          
        }
        
        // Fiscal Year
        
        for (int fy = 3; fy < 21; fy++) {

          String fiscalYear = "FY" + String.format("%02d", fy);
          
          log.info("Running Fiscal Year Entries: " + fiscalYear);
          
          String cqql = prepareCQQL("fiscal_year.cqql", new HashMap<String,String>(){
            {
              put("CQ_CONGRESS_NUMBER","116");
              put("CQ_FISCAL_YEAR", fiscalYear);
            }
          });
          
          extractAndInsert(restTemplate, apiBase, table, cqql, null, null, null, fiscalYear, null);
          
        }
        
      } catch (Exception e) {
        log.error("Failed to run runner",e);
        System.exit(1);
      }
      System.exit(0);
    };
  }


  /**
   * 
   * @param data
   * @return 
   */
  private static int countWords( String data )
  {
    String[] words = data.split( "(\\b[^\\s]+\\b)");
    return words.length;
  }
  
  
  /**
   * try removing tags with a simple regex
   * 
   * @param incoming
   * @return 
   */
  private static String stripTags( String incoming )
  {
    return incoming.replaceAll( "<[^>]*>", "" );
  }

  private void extractAndInsert(RestTemplate restTemplate, String apiBase, String table, String cqql, String approps, String commType, String committee, String fiscalYear, String legislativeType) {
    int index = 0;
    
    DocumentSearchRequest criteria = new DocumentSearchRequest();
    criteria.setCqql(cqql);
    criteria.setCount(500);
    criteria.setOffset(index);
    boolean hasNext = true;
    while (hasNext){
    criteria.setOffset(index);

      try{
        DocumentSearchResponse response = restTemplate.postForObject(apiBase + "docs" , criteria, DocumentSearchResponse.class);

        log.info("Total Found: " + response.getTotalFound());
        
        Integer totalRecords = response.getTotalFound();
        if(totalRecords > (index + 500)){
          index = index+500;
        } else {
          hasNext = false;
        }
        
        List<DocumentSearchHit> hits = response.getHits();
        for (DocumentSearchHit hit : hits) {
          DocumentMeta meta = hit.getMeta();  
          // Insert into table  DOC_UID          
          log.info("Doc UID: " + meta.getUid());
          
          try{
            
            Document document = restTemplate.getForObject(apiBase + "docs/document/{documentId}", Document.class, meta.getDocId());
            
            
            String documentContent = document.getContent().getBodyHtml();
            documentContent = stripTags(documentContent);
            
            Integer actualWordCnt = countWords(documentContent);
            
            // get Load ID
            String loadid = readTemplate.queryForObject("select load_id from document where document_id = ?", new Object[] {meta.getDocId()} , new RowMapper<String>() {

              @Override
              public String mapRow(
                ResultSet rs,
                int rowNum)
                  throws SQLException
              {
                return rs.getString("LOAD_ID");
              }
            });
            
            writeTemplate.update("insert into " + table + " (DOC_UID, "
                + "DOCUMENT_ID, "
                + "BILL_NUMBER, "
                + "OFF_TITLE, "
                + "VERSION_CODE, "
                + "VERSION_TYPE, "
                + "RECORD_ID, "
                + "APPROPRIATION, "
                + "CHAMBER, "
                + "COMMITTEE, "
                + "FISCAL_YEAR, "
                + "WORD_COUNT_META,"
                + "WORD_COUNT_ACTUAL,"
                + "LOAD_ID,"
                + "LEGISLATIVE_TYPE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                meta.getUid(),
                meta.getDocId(),
                meta.getBillNums() != null ? String.join(",", meta.getBillNums()) : "",
                meta.getOfficialTitle(),
                meta.getVersionCode(),
                meta.getVersionType(),
                meta.getRecordId(),
                approps,
                commType,
                committee,
                fiscalYear,
                meta.getWordCount(),
                actualWordCnt,
                loadid,
                legislativeType);
          } catch (Exception e){
            log.error("Unable to get the document from API: " + meta.getDocId(), e);
          }
        }
      }catch(Exception e){
        log.error("Document fetch failed for cqql: [" + cqql + "], with error: ", e);
//        System.exit(1);
      }
      
    }
  }
  
  private String prepareCQQL(String cqqlProperty, HashMap<String, String> replacers) {
    String cqql = env.getProperty(cqqlProperty);
    
    for (Entry<String, String> replacer : replacers.entrySet()) {
      cqql = cqql.replace(replacer.getKey(), replacer.getValue());
    }

    return cqql;
  }

  enum Chamber{
    HHSE("House"), SSEN("Senate");
    
    private String label;
    
    private Chamber(String label) {
      this.label = label;
    }
    
    public String getLabel() {
      return label;
    }
  }
  
  enum Committee{
    HBAN("House Financial Services"),SBUD("Senate Budget"),CONF("Conference Committee");
    
    private String label;
    
    private Committee(String label) {
      this.label = label;
    }
    
    public String getLabel() {
      return label;
    }
  }
  
  enum SubCommittee{
    HRAM05("Oversight & Investgations"),HBAN30("Financial Institutions & Consumer Credit"),HBAN50("Housing and Insurance"), HBAN60("Monetary Policy & Trade"), HBAN40("Oversight & Investigations"), HBAN70("Terrorism Financing Task Force"), HFIN10("Capital Markets..");
    
    private String label;
    
    private SubCommittee(String label) {
      this.label = label;
    }
    
    public String getLabel() {
      return label;
    }
  }
  

  
  enum LegislativeType {
    ONE("Reported, discharged or placed on a floor calendar","2"),TWO("Passed either house","4"), THREE("Passed both houses","5");
    
    private String text;
    
    private String value;
    
    
    private LegislativeType(String text, String value)
    {
      this.text=text;
      this.value = value;
    }


    public String getText()
    {
      return text;
    }


    public void setText(
      String text)
    {
      this.text = text;
    }


    public String getValue()
    {
      return value;
    }


    public void setValue(
      String value)
    {
      this.value = value;
    }
    
    
  }
  
  class Appropriation{
    String code;
    String label;
    public String getCode() {
      return code;
    }
    public void setCode(String code) {
      this.code = code;
    }
    public String getLabel() {
      return label;
      
      
    }
    public void setLabel(String label) {
      this.label = label;
    }
    
  }
}
